package ru.doctopus.spinkee;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;
import ru.doctopus.spinkee.R;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

public class Complex4Activity extends AppCompatActivity {

    private ImageView demonstrative;
    private VideoView videoDemonstrative;
    private TextView info;
    private Toolbar toolbar;

    private MediaPlayer mPlayer;

    private ArrayList<String> namesExercise = new ArrayList<>(); // названия упражнений комплекса 1
    private ArrayList<String> instructions = new ArrayList<>(); // информация упражнений комплекса 1
    private ArrayList<String> videosBoy = new ArrayList<>(); // видео с инструкциями (мальчик)
    private int currentExercise = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complex);

        setData(); // загружаем данные

        demonstrative = findViewById(R.id.demonstrative);
        videoDemonstrative = findViewById(R.id.videoDemonstrative);
        info = findViewById(R.id.info);
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                toolbar.setTitle(namesExercise.get(currentExercise));
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }

        // устанавливаем первое видео
        startVideo(0);
        info.setText(instructions.get(currentExercise));

        // изначально освобождаем ресурсы проигрывателя
        releaseMP();

        // создаем плеер и задаем источник
        mPlayer = MediaPlayer.create(this, R.raw.straighty_baby);
        mPlayer.setLooping(true);
        mPlayer.start();


    }


    // переход на следующее упражнение
    public void nextExercise(View view) {
        currentExercise++;
        Log.i("test", currentExercise+"");
        if (currentExercise == namesExercise.size()) {
            toolbar.setTitle(R.string.finish_exercise);
            videoDemonstrative.setVisibility(View.GONE);
            demonstrative.setVisibility(View.VISIBLE);
            demonstrative.setImageResource(R.drawable.monkey_face);
            info.setText(getString(R.string.praise));
            findViewById(R.id.btn).setVisibility(View.GONE);
            if (mPlayer.isPlaying())
                mPlayer.pause();
        } else
            updateUI();
    }


    // обновление интерфейса
    private void updateUI() {
        toolbar.setTitle(namesExercise.get(currentExercise));
        startVideo(currentExercise);
        info.setText(instructions.get(currentExercise));
    }


    private void startVideo(int index) {
        videoDemonstrative.stopPlayback();
        videoDemonstrative.setVideoURI(Uri.parse(videosBoy.get(index)));
        videoDemonstrative.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoDemonstrative.start();
    }


    // загружаем данные
    private void setData() {
        namesExercise.add(getString(R.string.complex_4_exercise_1_name));
        namesExercise.add(getString(R.string.complex_4_exercise_2_name));
        namesExercise.add(getString(R.string.complex_4_exercise_3_name));
        namesExercise.add(getString(R.string.complex_4_exercise_4_name));
        namesExercise.add(getString(R.string.complex_4_exercise_5_name));
        namesExercise.add(getString(R.string.complex_4_exercise_6_name));
        namesExercise.add(getString(R.string.complex_4_exercise_7_name));

        instructions.add(getString(R.string.complex_4_exercise_1_info));
        instructions.add(getString(R.string.complex_4_exercise_2_info));
        instructions.add(getString(R.string.complex_4_exercise_3_info));
        instructions.add(getString(R.string.complex_4_exercise_4_info));
        instructions.add(getString(R.string.complex_4_exercise_5_info));
        instructions.add(getString(R.string.complex_4_exercise_6_info));
        instructions.add(getString(R.string.complex_4_exercise_7_info));


        videosBoy.add("https://spinkee.net/video-server/toes-walk.mp4");
        videosBoy.add("https://spinkee.net/video-server/heels-walk.MP4");
        videosBoy.add("https://spinkee.net/video-server/outer-foot-walk.MP4");
        videosBoy.add("https://spinkee.net/video-server/toe-squats.MP4");
        videosBoy.add("https://spinkee.net/video-server/leg-bends.mp4");
        videosBoy.add("https://spinkee.net/video-server/scissors-on-back.mp4");
        videosBoy.add("https://spinkee.net/video-server/scissors-on-side.mp4");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    // освобождаем ресурсы проигрывателя при выходе из приложения
    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseMP();
    }


    // освобождаем ресурсы проигрывателя
    private void releaseMP() {
        if (mPlayer != null) {
            try {
                mPlayer.release();
                mPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}