package ru.doctopus.spinkee;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;
import ru.doctopus.spinkee.R;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class Complex2Activity extends AppCompatActivity {

    private ImageView demonstrative;
    private VideoView videoDemonstrative;
    private TextView info;
    private Toolbar toolbar;

    private MediaPlayer mPlayer;

    private ArrayList<String> namesExercise = new ArrayList<>(); // названия упражнений комплекса 1
    private ArrayList<String> instructions = new ArrayList<>(); // информация упражнений комплекса 1
    private ArrayList<String> videosBoy = new ArrayList<>(); // видео с инструкциями (мальчик)
    private int currentExercise = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complex);

        setData(); // загружаем данные

        demonstrative = findViewById(R.id.demonstrative);
        videoDemonstrative = findViewById(R.id.videoDemonstrative);
        info = findViewById(R.id.info);
        toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayShowTitleEnabled(false);
                toolbar.setTitle(namesExercise.get(currentExercise));
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }
        }

        // устанавливаем первое видео
        startVideo(0);
        info.setText(instructions.get(currentExercise));

        // изначально освобождаем ресурсы проигрывателя
        releaseMP();

        // создаем плеер и задаем источник
        mPlayer = MediaPlayer.create(this, R.raw.cloud_nine);
        mPlayer.setLooping(true);
        mPlayer.start();


    }


    // переход на следующее упражнение
    public void nextExercise(View view) {
        currentExercise++;
        Log.i("test", currentExercise+"");
        if (currentExercise == namesExercise.size()) {
            toolbar.setTitle(R.string.finish_exercise);
            videoDemonstrative.setVisibility(View.GONE);
            demonstrative.setVisibility(View.VISIBLE);
            demonstrative.setImageResource(R.drawable.monkey_face);
            info.setText(getString(R.string.praise));
            findViewById(R.id.btn).setVisibility(View.GONE);
            if (mPlayer.isPlaying())
                mPlayer.pause();
        } else
            updateUI();
    }


    // обновление интерфейса
    private void updateUI() {
        toolbar.setTitle(namesExercise.get(currentExercise));
        startVideo(currentExercise);
        info.setText(instructions.get(currentExercise));
    }


    private void startVideo(int index) {
        videoDemonstrative.stopPlayback();
        videoDemonstrative.setVideoURI(Uri.parse(videosBoy.get(index)));
        videoDemonstrative.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoDemonstrative.start();
    }


    // загружаем данные
    private void setData() {
        namesExercise.add(getString(R.string.complex_2_exercise_1_name));
        namesExercise.add(getString(R.string.complex_2_exercise_2_name));
        namesExercise.add(getString(R.string.complex_2_exercise_3_name));
        namesExercise.add(getString(R.string.complex_2_exercise_4_name));
        namesExercise.add(getString(R.string.complex_2_exercise_5_name));
        namesExercise.add(getString(R.string.complex_2_exercise_6_name));

        instructions.add(getString(R.string.complex_2_exercise_1_info));
        instructions.add(getString(R.string.complex_2_exercise_2_info));
        instructions.add(getString(R.string.complex_2_exercise_3_info));
        instructions.add(getString(R.string.complex_2_exercise_4_info));
        instructions.add(getString(R.string.complex_2_exercise_5_info));
        instructions.add(getString(R.string.complex_2_exercise_6_info));

        videosBoy.add("https://spinkee.net/video-server/fish.mp4");
        videosBoy.add("https://spinkee.net/video-server/boat.MP4");
        videosBoy.add("https://spinkee.net/video-server/boat-hands-locked.MP4");
        videosBoy.add("https://spinkee.net/video-server/reversed-plank.mp4");
        videosBoy.add("https://spinkee.net/video-server/sail-left.MP4");
        videosBoy.add("https://spinkee.net/video-server/superman.MP4");

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }


    // освобождаем ресурсы проигрывателя при выходе из приложения
    @Override
    public void onDestroy() {
        super.onDestroy();
        releaseMP();
    }


    // освобождаем ресурсы проигрывателя
    private void releaseMP() {
        if (mPlayer != null) {
            try {
                mPlayer.release();
                mPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}